package main

import (
	"fmt"
	"bufio"
	"os"
	"log"
)

type Local struct {
	pos int
	one int
	zero int
}

func Read(path string) []string {

	file, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var input []string

	for scanner.Scan() {
		input = append(input, scanner.Text())
	}

	return input
}


// Puzzle one
func CalculatePowerConsumption(input []string) int64 {

	var locals []Local
	for i := 0; i < len(input[0]); i++ {
		local := Local{i, 0, 0}
		locals = append(locals, local)
	}

	for _, str := range input {
		for index, chr := range str {
			if chr == '0' {
				locals[index].zero = locals[index].zero + 1
			} else {
				locals[index].one = locals[index].one + 1
			}
		}
	}
	

	var gamma int64
	var epsilon int64

	gamma = 0
	epsilon = 0

	var val_len = len(input[0]) - 1
	
	for _, local := range locals {
		if local.one > local.zero {
			gamma = gamma ^ (1 << (val_len - local.pos))
			epsilon = epsilon ^ (0 << (val_len - local.pos))
		} else {
			gamma = gamma ^ (0 << (val_len - local.pos)) 
			epsilon = epsilon ^ (1 << (val_len - local.pos))						
		}
	}

	return gamma * epsilon
	
}


// Puzzle 2
// There must be a way to do this better in go 
func ExtractRelevantEntries(input []string, value string, index int) []string {

	var result []string

	for _, str := range input {
		if str[index] == value[0] {
			result = append(result, str)
		}
	}

	return result
}

func ConvertToNumber(in string) int64 {

	var val int64
	val = 0

	for index, elm := range in {
		var elm_val int64
		if elm == '0' {
			elm_val = 0 
		} else {
			elm_val = 1
		}

		val = val ^ (elm_val << (len(in) - 1 - index))
	}

	return val	
}

func FindOxygenRating(input []string, index int) int64 {

	if len(input) == 0 {
		return -1
	}
	if len(input) == 1 || index == len(input[0]) {
		return ConvertToNumber(input[0])
	}
	
	var num_zeros int
	var num_ones int
	num_zeros = 0
	num_ones = 0

	for _, str := range input {
		if str[index] == '1' {
			num_ones = num_ones + 1
		} else {
			num_zeros = num_zeros + 1
		}
	}

	var cmp string = "1"

	if num_zeros != num_ones && num_zeros > num_ones {
		cmp = "0"
	}

	input = ExtractRelevantEntries(input, cmp, index)
	return FindOxygenRating(input, index + 1)	
}

func FindCO2Rating(input []string, index int) int64 {

	if len(input) == 0 {
		return -1
	}
	if len(input) == 1 || index == len(input[0]) {
		return ConvertToNumber(input[0])
	}
	
	var num_zeros int
	var num_ones int
	num_zeros = 0
	num_ones = 0

	for _, str := range input {
		if str[index] == '1' {
			num_ones = num_ones + 1
		} else {
			num_zeros = num_zeros + 1
		}
	}

	var cmp string = "0"

	if num_zeros != num_ones && num_zeros > num_ones {
		cmp = "1"
	}

	input = ExtractRelevantEntries(input, cmp, index)
	return FindCO2Rating(input, index + 1)	
}

func LifeSupportRating(input []string) int64 {
	return FindOxygenRating(input, 0) * FindCO2Rating(input, 0)
}

func main() {
	fmt.Println("Power consumption: ", CalculatePowerConsumption(Read("test_input.txt")))
	
	fmt.Println("Power consumption: ", CalculatePowerConsumption(Read("input.txt")))

	fmt.Println("Oxygen test: ", FindOxygenRating(Read("test_input.txt"), 0))
	fmt.Println("CO2 test: ", FindCO2Rating(Read("test_input.txt"), 0))
	fmt.Println("Life support rating test: ", LifeSupportRating(Read("test_input.txt")))
	fmt.Println("Life support rating: ", LifeSupportRating(Read("input.txt")))			


}



