package main

import (
	"fmt"
	"bufio"
	"os"
	"log"
	"strconv"
	"strings"
)

type Move struct {
	pos string
	val int
}

func Read(path string) []Move {

	file, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var input []Move

	for scanner.Scan() {
		line := strings.Split(scanner.Text(), " ")
		if len(line) < 2 {
			log.Fatal("Error a line is too short")
		}
		val, err := strconv.Atoi(line[1])

		if err != nil {
			log.Fatal("Could not convert value")
		}
		
		move := Move{line[0], val}
		input = append(input, move)
	}

	return input
}


// Puzzle one 
func FindHorizontalAndDepthMul(input []Move) int {

	var depth = 0
	var horizontal = 0
	
	for _, move := range input {
		if move.pos == "forward" {
			horizontal = horizontal + move.val
		} else if move.pos == "up" {
			depth = depth - move.val
		} else {
			depth = depth + move.val
		}
	}

	return depth * horizontal
}

// Puzzle two
func FindAimBasedHorizontalAndDepthMul(input []Move) int {

	var aim = 0
	
	var depth = 0
	var horizontal = 0
	
	for _, move := range input {
		if move.pos == "forward" {
			horizontal = horizontal + move.val
			depth = depth + (aim * move.val)
		} else if move.pos == "up" {
			aim = aim - move.val
		} else {
			aim = aim + move.val
		}
	}
	return depth * horizontal
}

func main() {
	fmt.Println("Multiplication of Horizontal and Depth: ", FindHorizontalAndDepthMul(Read("input.txt")))
	fmt.Println("Multiplication of Horizontal and Depth: ", FindAimBasedHorizontalAndDepthMul(Read("input.txt")))	
}
