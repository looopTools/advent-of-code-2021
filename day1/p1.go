package main

import (
	"fmt"
	"bufio"
	"log"
	"os"
	"strconv"
)

func Read(path string) []int {
	file, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	// If error we close the file
	defer file.Close()

	var values []int

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		val, err := strconv.Atoi(scanner.Text())

		if err != nil {
			log.Fatal("could not convert input to string")
		}
		values = append(values, val)
	}
	
	return values 
}





// Puzzle 1
// func findeNumIncreases(path string) int {

// 	// Read the file
// 	file, err := os.Open(path)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	// If error we close the file
// 	defer file.Close()

// 	scanner := bufio.NewScanner(file)

// 	var pre int
// 	pre = -1
	
// 	var ret = 0
// 	// We are reading while there is text
// 	// Go does not have a while loop concept
// 	for scanner.Scan() {

// 		current, err := strconv.Atoi(scanner.Text())

// 		if err != nil {
// 			log.Fatal("Could not convert input to string")
// 		}
		
// 		if pre == -1 {
// 			pre = current
// 		} else if current > pre{
// 			ret = ret + 1
// 		}
// 		pre = current
// 	}
	
// 	return ret
// }

// Puzzle 2
// func findSumIncrease(path string) int {

// 	file, err := os.Open(path)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	defer file.Close()

// 	scanner := bufio.NewScanner(file)

// 	var meassurements []int
	
// 	for scanner.Scan() {
// 		num, err := strconv.Atoi(scanner.Text())

// 		if err != nil {
// 			log.Fatal("Could not Convert to number")
// 		}

		
// 		meassurements = append(meassurements, num)
// 	}
	
// 	var sums[]int

// 	for index, elm := range meassurements {
// 		if index + 2 < len(meassurements) {
// 			sum := elm + meassurements[index + 1] + meassurements[index + 2]
// 			sums = append(sums, sum)
// 		}
// 	}

// 	var pre = -1 
// 	var ret = 0

// 	for _, elm := range sums {
// 		if pre != -1 && pre < elm {
// 			ret = ret + 1
// 		}
// 		pre = elm
// 	}

// 	return ret
// }

func findNumIncrease(values []int) int {

	var pre int
	pre = values[0]
	values = values[1:]
	ret := 0

	for _, elm := range values {
		if elm > pre {
			ret = ret + 1
		}
		pre = elm
	}

	return ret
}

func findSumIncrease(values []int) int {
	
	for index, elm := range values[:len(values) - 2] {
		values[index] = elm + values[index + 1] + values[index + 2] 
	}

	values = values[:len(values) - 2]
	var pre = values[0]
	var ret = 0
	for _, elm := range values[1:] {
		if elm > pre {
			ret = ret + 1
		}
		pre = elm
	}

	return ret
}

func main() {
	fmt.Println("Number of Single increase: ", findNumIncrease(Read("input.txt")))
	fmt.Println("Number of Sum increase: ", findSumIncrease(Read("input.txt")))
}
